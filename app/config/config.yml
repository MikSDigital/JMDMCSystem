imports:
    - { resource: parameters.php }
    - { resource: parameters.yml }
    - { resource: security.yml }
    - { resource: services.yml }

# Put parameters here that don't need to change on each machine where the app is deployed
# http://symfony.com/doc/current/best_practices/configuration.html#application-related-configuration
parameters:
    default_locale: ru
    fallback_locales: [ "ru", "en" ]

framework:
    #esi:             ~
    translator:      { fallbacks: "%fallback_locales%" }
    secret:          "%secret%"
    router:
        resource: "%kernel.root_dir%/config/routing.yml"
        strict_requirements: ~
    form:            ~
    csrf_protection: ~
    validation:      { enable_annotations: true }
    #serializer:      { enable_annotations: true }
    templating:
        engines: ['twig']
    default_locale:  "%default_locale%"
    trusted_hosts:   ~
    trusted_proxies: ~
    session:
        # http://symfony.com/doc/current/reference/configuration/framework.html#handler-id
        handler_id:  session.handler.native_file
        save_path:   "%kernel.root_dir%/../var/sessions/%kernel.environment%"
    fragments:       ~
    http_method_override: true
    assets: ~

assetic:
    debug:          '%kernel.debug%'
    use_controller: '%kernel.debug%'
    filters:
        cssrewrite: ~

# Twig Configuration
twig:
    debug:            "%kernel.debug%"
    strict_variables: "%kernel.debug%"
    form_themes:
        - 'form/fields.html.twig'
        - 'TetranzSelect2EntityBundle:Form:fields.html.twig'

# Doctrine Configuration
doctrine:
    dbal:
        default_connection: default
        connections:
            default:
                driver:   "%database_driver%"
                host:     "%database_host%"
                port:     "%database_port%"
                dbname:   "%database_name%"
                user:     "%database_user%"
                password: "%database_password%"
                charset:  UTF8
                # if using pdo_sqlite as your database driver:
                #   1. add the path in parameters.yml
                #     e.g. database_path: "%kernel.root_dir%/data/data.db3"
                #   2. Uncomment database_path in parameters.yml.dist
                #   3. Uncomment next line:
                #     path:     "%database_path%"

    orm:
        auto_generate_proxy_classes: "%kernel.debug%"
        naming_strategy: doctrine.orm.naming_strategy.underscore
        resolve_target_entities:
            Symfony\Component\Security\Core\User\UserInterface: JMD\MC\CoreBundle\Entity\UserProxy
        auto_mapping: true

# Doctrine Mongodb configuration
doctrine_mongodb:
    connections:
        default:
            server: '%mongodb_uri%'
            options: {}
    default_database: '%mongodb_database%'
    document_managers:
        default:
            auto_mapping: true
            filters:
                soft-deleteable:
                    class:  Gedmo\SoftDeleteable\Filter\ODM\SoftDeleteableFilter
                    enabled: true

# Gedmo doctrine extensions
stof_doctrine_extensions:
    default_locale: '%default_locale%'
    uploadable:
        validate_writable_directory:    true
    orm:
        default:
            sortable:       true
            softdeleteable: true
            timestampable:  true
    mongodb:
        default:
            sortable:       true
            softdeleteable: true
            timestampable:  true

# Swiftmailer Configuration
swiftmailer:
    transport:  "%mailer_transport%"
    host:       "%mailer_host%"
    username:   "%mailer_user%"
    password:   "%mailer_password%"
    encryption: "%mailer_encryption%"
    spool:     { type: memory }

knp_paginator:
    page_range: 5                      # default page range used in pagination control
    default_options:
        page_name: page                # page query parameter name
        sort_field_name: sort          # sort field query parameter name
        sort_direction_name: direction # sort direction query parameter name
        distinct: true                 # ensure distinct results, useful when ORM queries are using GROUP BY statements
    template:
        pagination: KnpPaginatorBundle:Pagination:twitter_bootstrap_v3_pagination.html.twig  # sliding pagination controls template
        sortable: :Layout/Pagination:sortable_link.html.twig # KnpPaginatorBundle:Pagination:sortable_link.html.twig # sort link template

monolog:
    handlers:
        security:
            type: stream
            path: "%kernel.logs_dir%/security-%kernel.environment%.log"
            level: info
            channels: ['security']

distribution_assets:
    components:
        bootstrap:               "vendor/twbs/bootstrap/dist"
        jquery:                  "vendor/components/jquery"
        jquery_ui:               "vendor/components/jqueryui"
        font_awesome:            "vendor/components/font-awesome"
        wysibb:                  "vendor/wbb/wysibb"
        select2:                 "vendor/github/select2/dist"
        select2_bootstrap:       "vendor/select2/select2-bootstrap-theme/dist"

liip_theme:
    themes: [ 'default' ]
    load_controllers: false
    assetic_integration: true

fm_bbcode:
    filter_sets:
        default_filter:
            strict: false # if you want to parse attr values without quotes
            locale: '%default_locale%'
            xhtml: true
            filters: [ default, block, quote, image, list, text, url, video, table, code ]

tetranz_select2_entity:
    minimum_input_length: 2
    page_limit: 8
    allow_clear: true
    delay: 500
    language: '%default_locale%'
    cache: false