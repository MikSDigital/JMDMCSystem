<?php

namespace Distribution\AssetsBundle\Composer;

use Composer\Script\Event;

use Sensio\Bundle\DistributionBundle\Composer\ScriptHandler as Handler;

class ScriptHandler extends Handler
{
    /**
     * @param Event $event
     */
    public static function installComponents(Event $event)
    {
        $options    = static::getOptions($event);
        $consoleDir = static::getConsoleDir($event, 'install components');

        if (null === $consoleDir) {
            return;
        }

        $webDir  = $options['symfony-web-dir'];
        $symlink = '';

        if ($options['symfony-assets-install'] == 'symlink') {
            $symlink = '--symlink ';
        } elseif ($options['symfony-assets-install'] == 'relative') {
            $symlink = '--symlink --relative ';
        }

        if (!static::hasDirectory($event, 'symfony-web-dir', $webDir, 'install components')) {
            return;
        }

        static::executeCommand(
            $event,
            $consoleDir,
            'components:install ' . $symlink . escapeshellarg($webDir),
            $options['process-timeout']
        );
    }
}
