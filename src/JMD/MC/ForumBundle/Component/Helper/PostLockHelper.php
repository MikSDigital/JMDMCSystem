<?php
namespace JMD\MC\ForumBundle\Component\Helper;

use JMD\MC\ForumBundle\Entity\Post;

class PostLockHelper
{
    /**
     *
     * @access protected
     * @var bool $enabled
     */
    protected $enabled;

    /**
     *
     * @access protected
     * @var int $afterDays
     */
    protected $afterDays;

    /**
     *
     * @access public
     * @param bool $enabled
     * @param int  $afterDays
     */
    public function __construct($enabled, $afterDays)
    {
        $this->enabled = $enabled;
        $this->afterDays = $afterDays;
    }

    /**
     *
     * @access public
     * @return bool
     */
    public function isEnabled()
    {
        return $this->enabled;
    }

    /**
     *
     * @access public
     * @param \JMD\MC\ForumBundle\Entity\Post $post
     */
    public function setLockLimitOnPost(Post $post)
    {
        $post->setUnlockedUntilDate(new \Datetime('now + ' . $this->afterDays . ' days'));
    }

    /**
     *
     * @access public
     * @param  \JMD\MC\ForumBundle\Entity\Post $post
     * @return bool
     */
    public function isLocked(Post $post)
    {
        if ($this->enabled) {
            return $post->isLocked();
        }

        return false;
    }
}
