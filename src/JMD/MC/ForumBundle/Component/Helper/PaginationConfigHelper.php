<?php
namespace JMD\MC\ForumBundle\Component\Helper;

class PaginationConfigHelper
{
    /**
     *
     * @var int $topicsPerPageOnSubscriptions
     */
    protected $topicsPerPageOnSubscriptions;

    /**
     *
     * @var int $topicsPerPageOnBoards
     */
    protected $topicsPerPageOnBoards;

    /**
     *
     * @var int $postsPerPageOnTopics
     */
    protected $postsPerPageOnTopics;

    /**
     *
     * @access public
     * @param int $topicsPerPageOnSubscriptions
     * @param int $topicsPerPageOnBoards
     * @param int $postsPerPageOnTopics
     */
    public function __construct($topicsPerPageOnSubscriptions, $topicsPerPageOnBoards, $postsPerPageOnTopics)
    {
        $this->topicsPerPageOnSubscriptions = $topicsPerPageOnSubscriptions;
        $this->topicsPerPageOnBoards        = $topicsPerPageOnBoards;
        $this->postsPerPageOnTopics         = $postsPerPageOnTopics;
    }

    /**
     *
     * @access public
     * @return int
     */
    public function getTopicsPerPageOnSubscriptions()
    {
        return $this->topicsPerPageOnSubscriptions;
    }

    /**
     *
     * @access public
     * @return int
     */
    public function getTopicsPerPageOnBoards()
    {
        return $this->topicsPerPageOnBoards;
    }

    /**
     *
     * @access public
     * @return int
     */
    public function getPostsPerPageOnTopics()
    {
        return $this->postsPerPageOnTopics;
    }
}
