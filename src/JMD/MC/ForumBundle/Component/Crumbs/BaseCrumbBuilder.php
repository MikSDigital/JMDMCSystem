<?php
namespace JMD\MC\ForumBundle\Component\Crumbs;

use JMD\MC\ForumBundle\Component\Crumbs\Factory\CrumbFactory;

class BaseCrumbBuilder
{
    /**
     *
     * @access protected
     * @var \JMD\MC\ForumBundle\Component\Crumbs\Factory\CrumbFactory $crumbFactory
     */
    protected $crumbFactory;

    /**
     *
     * @access public
     * @param \JMD\MC\ForumBundle\Component\Crumbs\Factory\CrumbFactory $crumbFactory
     */
    public function __construct(CrumbFactory $crumbFactory)
    {
        $this->crumbFactory = $crumbFactory;
    }

    public function createCrumbTrail()
    {
        return $this->crumbFactory->createNewCrumbTrail();
    }
}
