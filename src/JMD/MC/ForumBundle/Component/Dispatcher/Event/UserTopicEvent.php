<?php
namespace JMD\MC\ForumBundle\Component\Dispatcher\Event;

use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\HttpFoundation\Request;

use JMD\MC\ForumBundle\Entity\Topic;

class UserTopicEvent extends Event
{
    /**
     *
     * @access protected
     * @var \Symfony\Component\HttpFoundation\Request $request
     */
    protected $request;

    /**
     *
     * @access protected
     * @var \JMD\MC\ForumBundle\Entity\Topic $topic
     */
    protected $topic;

    /**
     *
     * @access protected
     * @var bool $subscribe
     */
    protected $subscribe;

    /**
     *
     * @access public
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param \JMD\MC\ForumBundle\Entity\Topic       $topic
     * @param bool                                      $subscribe
     */
    public function __construct(Request $request, Topic $topic = null, $subscribe = false)
    {
        $this->request = $request;
        $this->topic = $topic;
        $this->subscribe = $subscribe;
    }

    /**
     *
     * @access public
     * @return \Symfony\Component\HttpFoundation\Request
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     *
     * @access public
     * @return \JMD\MC\ForumBundle\Entity\Topic
     */
    public function getTopic()
    {
        return $this->topic;
    }

    /**
     *
     * @access public
     * @return bool
     */
    public function authorWantsToSubscribe()
    {
        return $this->subscribe;
    }
}
