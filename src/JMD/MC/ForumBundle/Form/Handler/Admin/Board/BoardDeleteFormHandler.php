<?php
namespace JMD\MC\ForumBundle\Form\Handler\Admin\Board;

use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\EventDispatcher\EventDispatcherInterface ;

use Doctrine\Common\Collections\ArrayCollection;

use JMD\MC\ForumBundle\Component\Dispatcher\ForumEvents;
use JMD\MC\ForumBundle\Component\Dispatcher\Event\AdminBoardEvent;
use JMD\MC\ForumBundle\Form\Handler\BaseFormHandler;
use JMD\MC\ForumBundle\Model\FrontModel\ModelInterface;
use JMD\MC\ForumBundle\Entity\Board;

class BoardDeleteFormHandler extends BaseFormHandler
{
    /**
     *
     * @access protected
     * @var \JMD\MC\ForumBundle\Form\Type\Admin\Board\BoardDeleteFormType $boardDeleteFormType
     */
    protected $boardDeleteFormType;

    /**
     *
     * @access protected
     * @var \JMD\MC\ForumBundle\Model\FrontModel\BoardModel $boardModel
     */
    protected $boardModel;

    /**
     *
     * @access protected
     * @var \JMD\MC\ForumBundle\Entity\Board $board
     */
    protected $board;

    /**
     *
     * @access public
     * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface      $dispatcher
     * @param \Symfony\Component\Form\FormFactory                              $factory
     * @param \JMD\MC\ForumBundle\Form\Type\Admin\Board\BoardDeleteFormType $boardDeleteFormType
     * @param \JMD\MC\ForumBundle\Model\FrontModel\BoardModel               $boardModel
     */
    public function __construct(EventDispatcherInterface  $dispatcher, FormFactory $factory, $boardDeleteFormType, ModelInterface $boardModel)
    {
        $this->dispatcher = $dispatcher;
        $this->factory = $factory;
        $this->boardDeleteFormType = $boardDeleteFormType;
        $this->boardModel = $boardModel;
    }

    /**
     *
     * @access public
     * @param  \JMD\MC\ForumBundle\Entity\Board                                    $board
     * @return \JMD\MC\ForumBundle\Form\Handler\Admin\Board\BoardDeleteFormHandler
     */
    public function setBoard(Board $board)
    {
        $this->board = $board;

        return $this;
    }

    /**
     *
     * @access public
     * @return \Symfony\Component\Form\Form
     */
    public function getForm()
    {
        if (null == $this->form) {
            if (!is_object($this->board) && !$this->board instanceof Board) {
                throw new \Exception('Board object must be specified to delete.');
            }

            $this->dispatcher->dispatch(ForumEvents::ADMIN_BOARD_DELETE_INITIALISE, new AdminBoardEvent($this->request, $this->board));

            $this->form = $this->factory->create(get_class($this->boardDeleteFormType), $this->board);
        }

        return $this->form;
    }

    /**
     *
     * @access protected
     * @param \JMD\MC\ForumBundle\Entity\Board $board
     */
    protected function onSuccess(Board $board)
    {
        $this->dispatcher->dispatch(ForumEvents::ADMIN_BOARD_DELETE_SUCCESS, new AdminBoardEvent($this->request, $board));

        if (! $this->form->get('confirm_subordinates')->getData()) {
            $topics = new ArrayCollection($board->getTopics()->toArray());

            $this->boardModel->reassignTopicsToBoard($topics, null)->flush();
        }

        $this->boardModel->deleteBoard($board);

        $this->dispatcher->dispatch(ForumEvents::ADMIN_BOARD_DELETE_COMPLETE, new AdminBoardEvent($this->request, $board));
    }
}
