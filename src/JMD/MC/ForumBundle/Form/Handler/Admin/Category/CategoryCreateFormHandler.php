<?php
namespace JMD\MC\ForumBundle\Form\Handler\Admin\Category;

use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\EventDispatcher\EventDispatcherInterface ;

use JMD\MC\ForumBundle\Component\Dispatcher\ForumEvents;
use JMD\MC\ForumBundle\Component\Dispatcher\Event\AdminCategoryEvent;
use JMD\MC\ForumBundle\Form\Handler\BaseFormHandler;
use JMD\MC\ForumBundle\Model\FrontModel\ModelInterface;
use JMD\MC\ForumBundle\Entity\Forum;
use JMD\MC\ForumBundle\Entity\Category;

class CategoryCreateFormHandler extends BaseFormHandler
{
    /**
     *
     * @access protected
     * @var \JMD\MC\ForumBundle\Form\Type\Admin\Category\CategoryCreateFormType $categoryCreateFormType
     */
    protected $categoryCreateFormType;

    /**
     *
     * @access protected
     * @var \JMD\MC\ForumBundle\Model\FrontModel\CategoryModel $categoryModel
     */
    protected $categoryModel;

    /**
     *
     * @access protected
     * @var \JMD\MC\ForumBundle\Entity\Forum $defaultForum
     */
    protected $defaultForum;

    /**
     *
     * @access public
     * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface            $dispatcher
     * @param \Symfony\Component\Form\FormFactory                                    $factory
     * @param \JMD\MC\ForumBundle\Form\Type\Admin\Category\CategoryCreateFormType $categoryCreateFormType
     * @param \JMD\MC\ForumBundle\Model\FrontModel\CategoryModel                  $categoryModel
     */
    public function __construct(EventDispatcherInterface $dispatcher, FormFactory $factory, $categoryCreateFormType, ModelInterface $categoryModel)
    {
        $this->dispatcher = $dispatcher;
        $this->factory = $factory;
        $this->categoryCreateFormType = $categoryCreateFormType;
        $this->categoryModel = $categoryModel;
    }

    /**
     *
     * @access public
     * @param  \JMD\MC\ForumBundle\Entity\Forum                                          $forum
     * @return \JMD\MC\ForumBundle\Form\Handler\Admin\Category\CategoryCreateFormHandler
     */
    public function setDefaultForum(Forum $forum)
    {
        $this->defaultForum = $forum;

        return $this;
    }

    /**
     *
     * @access public
     * @return \Symfony\Component\Form\Form
     */
    public function getForm()
    {
        if (null == $this->form) {
            $category = $this->categoryModel->createCategory();

            $options = array(
                'default_forum' => $this->defaultForum
            );

            $this->dispatcher->dispatch(ForumEvents::ADMIN_CATEGORY_CREATE_INITIALISE, new AdminCategoryEvent($this->request, $category));

            $this->form = $this->factory->create(get_class($this->categoryCreateFormType), $category, $options);
        }

        return $this->form;
    }

    /**
     *
     * @access protected
     * @param \JMD\MC\ForumBundle\Entity\Category $category
     */
    protected function onSuccess(Category $category)
    {
        $this->dispatcher->dispatch(ForumEvents::ADMIN_CATEGORY_CREATE_SUCCESS, new AdminCategoryEvent($this->request, $category));

        $this->categoryModel->saveCategory($category);

        $this->dispatcher->dispatch(ForumEvents::ADMIN_CATEGORY_CREATE_COMPLETE, new AdminCategoryEvent($this->request, $category));
    }
}
