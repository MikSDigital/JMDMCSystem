<?php
namespace JMD\MC\ForumBundle\Form\Type\Admin\Board;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;

class BoardCreateFormType extends AbstractType
{
    /**
     *
     * @access protected
     * @var string $boardClass
     */
    protected $boardClass;

    /**
     *
     * @access protected
     * @var string $categoryClass
     */
    protected $categoryClass;

    /**
     *
     * @access protected
     * @var Object $roleHelper
     */
    protected $roleHelper;

    /**
     *
     * @access public
     * @param string $boardClass
     * @param string $categoryClass
     * @param Object $roleHelper
     */
    public function __construct($boardClass, $categoryClass, $roleHelper)
    {
        $this->boardClass = $boardClass;
        $this->categoryClass = $categoryClass;
        $this->roleHelper = $roleHelper;
    }

    /**
     *
     * @access public
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('category', EntityType::class,
                array(
                    'class'              => $this->categoryClass,
                    'group_by'           => 'forum.name',
                    'query_builder'      =>
                        function (EntityRepository $er) {
                            return $er
                                ->createQueryBuilder('c')
                                ->leftJoin('c.forum', 'f')
                                //->groupBy('c.forum')
                            ;
                        },
                    'data'               => $options['default_category'],
                    'required'           => false,
                    'label'              => 'category.label',
                    'translation_domain' => 'JMDMCForumBundle',
                )
            )
            ->add('name', TextType::class,
                array(
                    'label'              => 'board.name-label',
                    'translation_domain' => 'JMDMCForumBundle',
                )
            )
            ->add('description', TextareaType::class,
                array(
                    'label'              => 'board.description-label',
                    'translation_domain' => 'JMDMCForumBundle',
                )
            )
            ->add('readAuthorisedRoles', ChoiceType::class,
                array(
                    'required'           => false,
                    'expanded'           => true,
                    'multiple'           => true,
                    'choices'            => $options['available_roles'],
                    'label'              => 'board.roles.topic-view-label',
                    'translation_domain' => 'JMDMCForumBundle',
                )
            )
            ->add('topicCreateAuthorisedRoles', ChoiceType::class,
                array(
                    'required'           => false,
                    'expanded'           => true,
                    'multiple'           => true,
                    'choices'            => $options['available_roles'],
                    'label'              => 'board.roles.topic-create-label',
                    'translation_domain' => 'JMDMCForumBundle',
                )
            )
            ->add('topicReplyAuthorisedRoles', ChoiceType::class,
                array(
                    'required'           => false,
                    'expanded'           => true,
                    'multiple'           => true,
                    'choices'            => $options['available_roles'],
                    'label'              => 'board.roles.topic-reply-label',
                    'translation_domain' => 'JMDMCForumBundle',
                )
            )
        ;
    }

    /**
     *
     * @access public
     * @param \Symfony\Component\OptionsResolver\OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'          => $this->boardClass,
            'csrf_protection'     => true,
            'csrf_field_name'     => '_token',
            // a unique key to help generate the secret token
            'intention'           => 'forum_board_create_item',
            'validation_groups'   => array('forum_board_create'),
            'cascade_validation'  => true,
            'available_roles'     => $this->roleHelper->getRoleHierarchy(),
            'default_category'    => null
        ));
    }

    /**
     *
     * @access public
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'Forum_BoardCreate';
    }
}
