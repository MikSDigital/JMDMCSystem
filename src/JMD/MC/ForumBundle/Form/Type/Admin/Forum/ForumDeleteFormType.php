<?php
namespace JMD\MC\ForumBundle\Form\Type\Admin\Forum;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormError;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\NotBlank;

class ForumDeleteFormType extends AbstractType
{
    /**
     *
     * @access protected
     * @var string $forumClass
     */
    protected $forumClass;

    /**
     *
     * @access public
     * @param string $forumClass
     */
    public function __construct($forumClass)
    {
        $this->forumClass = $forumClass;
    }

    /**
     *
     * @access public
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $trueValidator = function (FormEvent $event) {
            $form = $event->getForm();

            $confirm = $form->get('confirm_delete')->getData();

            if (empty($confirm) || $confirm == false) {
                $form['confirm_delete']->addError(new FormError("You must confirm this action."));
            }
        };

        $builder
            ->add('confirm_delete', CheckboxType::class,
                array(
                    'mapped'             => false,
                    'required'           => true,
                    'label'              => 'forum.confirm-delete-label',
                    'translation_domain' => 'JMDMCForumBundle',
                    'constraints'        => array(
                        new IsTrue(),
                        new NotBlank()
                    ),
                )
            )
            ->add('confirm_subordinates', CheckboxType::class,
                array(
                    'mapped'             => false,
                    'required'           => true,
                    'label'              => 'forum.confirm-delete-subordinates-label',
                    'translation_domain' => 'JMDMCForumBundle',
                    'constraints'        => array(
                        new IsTrue(),
                        new NotBlank()
                    ),
                )
            )
            ->addEventListener(FormEvents::POST_SET_DATA, $trueValidator)
        ;
    }

    /**
     *
     * @access public
     * @param \Symfony\Component\OptionsResolver\OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'          => $this->forumClass,
            'csrf_protection'     => true,
            'csrf_field_name'     => '_token',
            // a unique key to help generate the secret token
            'intention'           => 'forum_forum_delete_item',
            'validation_groups'   => array('forum_forum_delete'),
            'cascade_validation'  => true,
        ));
    }

    /**
     *
     * @access public
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'Forum_ForumDelete';
    }
}
