<?php
namespace JMD\MC\ForumBundle\Model\FrontModel;

use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use JMD\MC\ForumBundle\Model\Component\Manager\ManagerInterface;
use JMD\MC\ForumBundle\Model\Component\Repository\RepositoryInterface;

/**
 *
 * @category matuck
 * @package  ForumBundle
 *
 * @author   Mitch Tuck <matuck@matuck.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @version  Release: 2.0
 * @link     https://github.com/matuck/JMDMCForumBundle
 *
 * @abstract
 */
abstract class BaseModel
{
    /**
     *
     * @access protected
     * @var \JMD\MC\ForumBundle\Model\Component\Repository\RepositoryInterface
     */
    protected $repository;

    /**
     *
     * @access protected
     * @var \JMD\MC\ForumBundle\Model\Component\Manager\ManagerInterface
     */
    protected $manager;

    /**
     *
     * @access protected
     * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface $dispatcher
     */
    protected $dispatcher;

    /**
     *
     * @access public
     * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface           $dispatcher
     * @param \JMD\MC\ForumBundle\Model\Component\Repository\RepositoryInterface $repository
     * @param \JMD\MC\ForumBundle\Model\Component\Manager\ManagerInterface       $manager
     */
    public function __construct(EventDispatcherInterface $dispatcher, RepositoryInterface $repository, ManagerInterface $manager)
    {
        $this->dispatcher = $dispatcher;

        $repository->setModel($this);
        $this->repository = $repository;

        $manager->setModel($this);
        $this->manager = $manager;
    }

    /**
     *
     * @access public
     * @return \JMD\MC\ForumBundle\Model\Component\Repository\RepositoryInterface
     */
    public function getRepository()
    {
        return $this->repository;
    }

    /**
     *
     * @access public
     * @return \JMD\MC\ForumBundle\Model\Component\Manager\ManagerInterface
     */
    public function getManager()
    {
        return $this->manager;
    }
}
