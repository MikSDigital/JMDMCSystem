<?php
namespace JMD\MC\ForumBundle\Model\FrontModel;

use Doctrine\Common\Collections\ArrayCollection;
use JMD\MC\ForumBundle\Model\FrontModel\BaseModel;
use JMD\MC\ForumBundle\Model\FrontModel\ModelInterface;
use JMD\MC\ForumBundle\Entity\Board;

class BoardModel extends BaseModel implements ModelInterface
{
    /**
     *
     * @access public
     * @return \JMD\MC\ForumBundle\Entity\Board
     */
    public function createBoard()
    {
        return $this->getManager()->createBoard();
    }

    /**
     *
     * @access public
     * @return \Doctrine\Common\Collection\ArrayCollection
     */
    public function findAllBoards()
    {
        return $this->getRepository()->findAllBoards();
    }

    /**
     *
     * @access public
     * @param  int                                          $categoryId
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function findAllBoardsForCategoryById($categoryId)
    {
        return $this->getRepository()->findAllBoardsForCategoryById($categoryId);
    }

    /**
     *
     * @access public
     * @param  int                                          $forumId
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function findAllBoardsForForumById($forumId)
    {
        return $this->getRepository()->findAllBoardsForForumById($forumId);
    }

    /**
     *
     * @access public
     * @param  int                                 $boardId
     * @return \JMD\MC\ForumBundle\Entity\Board
     */
    public function findOneBoardById($boardId)
    {
        return $this->getRepository()->findOneBoardById($boardId);
    }

    /**
     *
     * @access public
     * @param  int                                 $boardId
     * @return \JMD\MC\ForumBundle\Entity\Board
     */
    public function findOneBoardByIdWithCategory($boardId)
    {
        return $this->getRepository()->findOneBoardByIdWithCategory($boardId);
    }

    /**
     *
     * @access public
     * @return Array
     */
    public function getBoardCount()
    {
        return $this->getRepository()->getBoardCount();
    }

    /**
     *
     * @access public
     * @param  \JMD\MC\ForumBundle\Entity\Board                             $board
     * @return \JMD\MC\ForumBundle\Model\Component\Manager\ManagerInterface
     */
    public function saveBoard(Board $board)
    {
        return $this->getManager()->saveBoard($board);
    }

    /**
     *
     * @access public
     * @param  \JMD\MC\ForumBundle\Entity\Board                             $board
     * @return \JMD\MC\ForumBundle\Model\Component\Manager\ManagerInterface
     */
    public function updateBoard(Board $board)
    {
       return $this->getManager()->updateBoard($board);
    }

    /**
     *
     * @access public
     * @param  \JMD\MC\ForumBundle\Entity\Board                             $board
     * @return \JMD\MC\ForumBundle\Model\Component\Manager\ManagerInterface
     */
    public function deleteBoard(Board $board)
    {
        return $this->getManager()->deleteBoard($board);
    }

    /**
     *
     * @access public
     * @param  \Doctrine\Common\Collections\ArrayCollection                    $topics
     * @param  \JMD\MC\ForumBundle\Entity\Board                             $board
     * @return \JMD\MC\ForumBundle\Model\Component\Manager\ManagerInterface
     */
    public function reassignTopicsToBoard(ArrayCollection $topics, Board $board = null)
    {
        return $this->getManager()->reassignTopicsToBoard($topics, $board);
    }

    /**
     *
     * @access public
     * @param  Array                                                           $boards
     * @param  \JMD\MC\ForumBundle\Entity\Board                             $boardShift
     * @param  int                                                             $direction
     * @return \JMD\MC\ForumBundle\Model\Component\Manager\ManagerInterface
     */
    public function reorderBoards($boards, Board $boardShift, $direction)
    {
        return $this->getManager()->reorderBoards($boards, $boardShift, $direction);
    }
}
