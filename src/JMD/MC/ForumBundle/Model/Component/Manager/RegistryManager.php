<?php
namespace JMD\MC\ForumBundle\Model\Component\Manager;

use JMD\MC\ForumBundle\Model\Component\Manager\ManagerInterface;
use JMD\MC\ForumBundle\Model\Component\Manager\BaseManager;
use JMD\MC\ForumBundle\Entity\Registry;

class RegistryManager extends BaseManager implements ManagerInterface
{
    /**
     *
     * @access public
     * @return \JMD\MC\ForumBundle\Entity\Registry
     */
    public function createRegistry()
    {
        return $this->gateway->createRegistry();
    }

    /**
     *
     * @access public
     * @param  \JMD\MC\ForumBundle\Entity\Registry                         $registry
     * @return \JMD\MC\ForumBundle\Model\Component\Manager\RegistryManager
     */
    public function saveRegistry(Registry $registry)
    {
        $this->gateway->saveRegistry($registry);

        return $this;
    }
}
