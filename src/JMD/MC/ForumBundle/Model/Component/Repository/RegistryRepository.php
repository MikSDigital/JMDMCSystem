<?php
namespace JMD\MC\ForumBundle\Model\Component\Repository;

use JMD\MC\ForumBundle\Model\Component\Repository\Repository;
use JMD\MC\ForumBundle\Model\Component\Repository\RepositoryInterface;

/**
 * RegistryRepository
 *
 * @category matuck
 * @package  ForumBundle
 *
 * @author   Mitch Tuck <matuck@matuck.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @version  Release: 2.0
 * @link     https://github.com/matuck/JMDMCForumBundle
 */
class RegistryRepository extends BaseRepository implements RepositoryInterface
{
    /**
     *
     * @access public
     * @param  int                                    $userId
     * @return \JMD\MC\ForumBundle\Entity\Registry
     */
    public function findOneRegistryForUserById($userId)
    {
        if (null == $userId || ! is_numeric($userId) || $userId == 0) {
            throw new \Exception('User id "' . $userId . '" is invalid!');
        }

        $params = array(':userId' => $userId);

        $qb = $this->createSelectQuery(array('r'));

        $qb->where('r.ownedBy = :userId');

        return $this->gateway->findRegistry($qb, $params);
    }
}
