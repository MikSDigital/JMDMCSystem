<?php
namespace JMD\MC\ForumBundle\Model\Component\Gateway;

use Doctrine\ORM\QueryBuilder;
use JMD\MC\ForumBundle\Model\Component\Gateway\GatewayInterface;
use JMD\MC\ForumBundle\Model\Component\Gateway\BaseGateway;
use JMD\MC\ForumBundle\Entity\Topic;

class TopicGateway extends BaseGateway implements GatewayInterface
{
    /**
     *
     * @access private
     * @var string $queryAlias
     */
    protected $queryAlias = 't';

    /**
     *
     * @access public
     * @param  \Doctrine\ORM\QueryBuilder                   $qb
     * @param  Array                                        $parameters
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function findTopic(QueryBuilder $qb = null, $parameters = null)
    {
        if (null == $qb) {
            $qb = $this->createSelectQuery();
        }

        return $this->one($qb, $parameters);
    }

    /**
     *
     * @access public
     * @param  \Doctrine\ORM\QueryBuilder                   $qb
     * @param  Array                                        $parameters
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function findTopics(QueryBuilder $qb = null, $parameters = null)
    {
        if (null == $qb) {
            $qb = $this->createSelectQuery();
        }

        return $this->all($qb, $parameters);
    }

    /**
     *
     * @access public
     * @param  \Doctrine\ORM\QueryBuilder $qb
     * @param  Array                      $parameters
     * @return int
     */
    public function countTopics(QueryBuilder $qb = null, $parameters = null)
    {
        if (null == $qb) {
            $qb = $this->createCountQuery();
        }

        if (null == $parameters) {
            $parameters = array();
        }

        $qb->setParameters($parameters);

        try {
            return $qb->getQuery()->getSingleScalarResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return 0;
        }
    }

    /**
     *
     * @access public
     * @param  \JMD\MC\ForumBundle\Entity\Topic                             $topic
     * @return \JMD\MC\ForumBundle\Model\Component\Gateway\GatewayInterface
     */
    public function saveTopic(Topic $topic)
    {
        $this->persist($topic)->flush();

        return $this;
    }

    /**
     *
     * @access public
     * @param  \JMD\MC\ForumBundle\Entity\Topic                             $topic
     * @return \JMD\MC\ForumBundle\Model\Component\Gateway\GatewayInterface
     */
    public function updateTopic(Topic $topic)
    {
        $this->persist($topic)->flush();

        return $this;
    }

    /**
     *
     * @access public
     * @param  \JMD\MC\ForumBundle\Entity\Topic                             $topic
     * @return \JMD\MC\ForumBundle\Model\Component\Gateway\GatewayInterface
     */
    public function deleteTopic(Topic $topic)
    {
        $this->remove($topic)->flush();

        return $this;
    }

    /**
     *
     * @access public
     * @return \JMD\MC\ForumBundle\Entity\Topic
     */
    public function createTopic()
    {
        return new $this->entityClass();
    }
}
