<?php
namespace JMD\MC\ForumBundle\Controller;

use JMD\MC\ForumBundle\Entity\Forum;
use JMD\MC\ForumBundle\Entity\Topic;

class ModeratorTopicBaseController extends BaseController
{
    /**
     *
     * @access protected
     * @param  \JMD\MC\ForumBundle\Entity\Topic                        $topic
     * @return \JMD\MC\ForumBundle\Form\Handler\TopicCreateFormHandler
     */
    protected function getFormHandlerToDeleteTopic(Topic $topic)
    {
        $formHandler = $this->container->get('jmdmc_forum.form.handler.topic_delete');

        $formHandler->setTopic($topic);
        $formHandler->setUser($this->getUser());
        $formHandler->setRequest($this->getRequest());

        return $formHandler;
    }

    /**
     *
     * @access protected
     * @param  \JMD\MC\ForumBundle\Entity\Forum                             $forum
     * @param  \JMD\MC\ForumBundle\Entity\Topic                             $topic
     * @return \JMD\MC\ForumBundle\Form\Handler\TopicChangeBoardFormHandler
     */
    protected function getFormHandlerToChangeBoardOnTopic(Forum $forum, Topic $topic)
    {
        $formHandler = $this->container->get('jmdmc_forum.form.handler.change_topics_board');

        $formHandler->setForum($forum);
        $formHandler->setTopic($topic);
        $formHandler->setRequest($this->getRequest());

        return $formHandler;
    }
}
