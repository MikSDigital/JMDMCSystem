<?php
namespace JMD\MC\ForumBundle\Controller;

use JMD\MC\ForumBundle\Entity\Category;

class AdminCategoryBaseController extends BaseController
{
    /**
     *
     * @access protected
     * @return \JMD\MC\ForumBundle\Form\Handler\CategoryCreateFormHandler
     */
    protected function getFormHandlerToCreateCategory($forumFilter = null)
    {
        $formHandler = $this->container->get('jmdmc_forum.form.handler.category_create');

        $formHandler->setRequest($this->getRequest());

        if ($forumFilter) {
            $forum = $this->getForumModel()->findOneForumById($forumFilter);

            if ($forum) {
                $formHandler->setDefaultForum($forum);
            }
        }

        return $formHandler;
    }

    /**
     *
     * @access protected
     * @return \JMD\MC\ForumBundle\Form\Handler\CategoryUpdateFormHandler
     */
    protected function getFormHandlerToUpdateCategory(Category $category)
    {
        $formHandler = $this->container->get('jmdmc_forum.form.handler.category_update');

        $formHandler->setRequest($this->getRequest());

        $formHandler->setCategory($category);

        return $formHandler;
    }

    /**
     *
     * @access protected
     * @return \JMD\MC\ForumBundle\Form\Handler\CategoryDeleteFormHandler
     */
    protected function getFormHandlerToDeleteCategory(Category $category)
    {
        $formHandler = $this->container->get('jmdmc_forum.form.handler.category_delete');

        $formHandler->setRequest($this->getRequest());

        $formHandler->setCategory($category);

        return $formHandler;
    }

    /**
     *
     * @access protected
     * @param  \JMD\MC\ForumBundle\Entity\Category $category
     * @return array
     */
    protected function getFilterQueryStrings(Category $category)
    {
        $params = array();

        if ($category->getForum()) {
            $params['forum_filter'] = $category->getForum()->getId();
        }

        return $params;
    }
}
