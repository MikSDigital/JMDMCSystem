<?php
namespace JMD\MC\ForumBundle\Controller;

class UserCategoryController extends BaseController
{
    /**
     *
     * Has 2 modes, when forum name is specified, categories for that forum are listed.
     * But, when forumName is ommitted all unassigned categories are listed.
     *
     * Optional Defaults:
     * default = unassigned categories only (will not include categories assigned to a forum, acts as default).
     *
     * @access public
     * @param  string         $forumName
     * @return RenderResponse
     */
    public function indexAction($forumName)
    {
        $forums = $this->getForumModel()->findAllForums();
        $categories = [];

        $forum = null;
        foreach ($forums as $forum) {
            $this->isAuthorised($this->getAuthorizer()->canShowForum($forum));
            $forumName = $forum->getName();
            $categories = array_merge_recursive($categories,
                $this->getCategoryModel()->findAllCategoriesWithBoardsForForumByName($forumName));
        }

        return $this->renderResponse('JMDMCForumBundle:User:Category/index.html.', array(
            'crumbs' => $this->getCrumbs()->addUserCategoryIndex($forum),
            'forum' => $forum,
            'forumName' => $forumName,
            'categories' => $categories,
            'topics_per_page' => $this->container->getParameter('jmdmc_forum.board.user.show.topics_per_page'),
        ));
    }

    /**
     *
     * @access public
     * @param  string         $forumName
     * @param  int            $categoryId
     * @return RenderResponse
     */
    public function showAction($forumName, $categoryId)
    {
        $this->isFound($forum = $this->getForumModel()->findOneForumByName($forumName));
        $this->isFound($category = $this->getCategoryModel()->findOneCategoryByIdWithBoards($categoryId));
        $this->isAuthorised($this->getAuthorizer()->canShowCategory($category, $forum));

        return $this->renderResponse('JMDMCForumBundle:User:Category/show.html.', array(
            'crumbs' => $this->getCrumbs()->addUserCategoryShow($forum, $category),
            'forum' => $forum,
            'forumName' => $forumName,
            'category' => $category,
            'categories' => array($category),
            'topics_per_page' => $this->container->getParameter('jmdmc_forum.board.user.show.topics_per_page'),
        ));
    }
}
