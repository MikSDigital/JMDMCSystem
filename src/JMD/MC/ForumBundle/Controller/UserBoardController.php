<?php
namespace JMD\MC\ForumBundle\Controller;

class UserBoardController extends BaseController
{
    /**
     *
     * @access public
     * @param  string                          $forumName
     * @param  int                             $boardId
     * @return RedirectResponse|RenderResponse
     */
    public function showAction($forumName, $boardId)
    {
        $this->isFound($forum = $this->getForumModel()->findOneForumByName($forumName));
        $this->isFound($board = $this->getBoardModel()->findOneBoardByIdWithCategory($boardId));
        $this->isAuthorised($this->getAuthorizer()->canShowBoard($board, $forum));
        $itemsPerPage = $this->getPageHelper()->getTopicsPerPageOnBoards();
        $stickyTopics = $this->getTopicModel()->findAllTopicsStickiedByBoardId($boardId, true);
        $topicsPager = $this->getTopicModel()->findAllTopicsPaginatedByBoardId($boardId, $this->getQuery('page', 1), $itemsPerPage, true);

        return $this->renderResponse('JMDMCForumBundle:User:Board/show.html.', array(
            'crumbs' => $this->getCrumbs()->addUserBoardShow($forum, $board),
            'forum' => $forum,
            'forumName' => $forumName,
            'board' => $board,
            'pager' => $topicsPager,
            'posts_per_page' => $this->container->getParameter('jmdmc_forum.topic.user.show.posts_per_page'), // for working out last page per topic.
            'sticky_topics' => $stickyTopics,
        ));
    }
}
