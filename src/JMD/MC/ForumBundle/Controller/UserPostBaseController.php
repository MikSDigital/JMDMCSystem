<?php
namespace JMD\MC\ForumBundle\Controller;

use JMD\MC\ForumBundle\Entity\Post;

class UserPostBaseController extends BaseController
{
    /**
     *
     * @access protected
     * @param  \JMD\MC\ForumBundle\Entity\Post                        $post
     * @return \JMD\MC\ForumBundle\Form\Handler\PostUpdateFormHandler
     */
    protected function getFormHandlerToEditPost(Post $post)
    {
        // If post is the very first post of the topic then use a topic handler so user can change topic title.
        if ($post->getTopic()->getFirstPost()->getId() == $post->getId()) {
            $formHandler = $this->container->get('jmdmc_forum.form.handler.topic_update');
        } else {
            $formHandler = $this->container->get('jmdmc_forum.form.handler.post_update');
        }

        $formHandler->setPost($post);
        $formHandler->setUser($this->getUser());
        $formHandler->setRequest($this->getRequest());

        return $formHandler;
    }

    /**
     *
     * @access protected
     * @param  \JMD\MC\ForumBundle\Entity\Post                        $post
     * @return \JMD\MC\ForumBundle\Form\Handler\PostDeleteFormHandler
     */
    protected function getFormHandlerToDeletePost(Post $post)
    {
        $formHandler = $this->container->get('jmdmc_forum.form.handler.post_delete');

        $formHandler->setPost($post);
        $formHandler->setUser($this->getUser());
        $formHandler->setRequest($this->getRequest());

        return $formHandler;
    }
}
