<?php
namespace JMD\MC\ForumBundle\Entity;

use JMD\MC\ForumBundle\Entity\Model\Forum as AbstractForum;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\SecurityContextInterface;

class Forum extends AbstractForum
{
    /**
     *
     * @var integer $id
     */
    protected $id;

    /**
     *
     * @var integer $id
     */
    protected $name;

    /**
     *
     * @var array $readAuthorisedRoles
     */
    protected $readAuthorisedRoles;

    /**
     *
     * @access public
     */
    public function __construct()
    {
        parent::__construct();

        // your own logic
        $this->readAuthorisedRoles = array();
    }

    public function __toString()
    {
        return $this->name;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @return Forum
     */
    public function setName($name)
    {
        return $this->name = $name;
    }

    /**
     *
     * @return array
     */
    public function getReadAuthorisedRoles()
    {
        return $this->readAuthorisedRoles;
    }

    /**
     *
     * @param  array $roles
     * @return Board
     */
    public function setReadAuthorisedRoles(array $roles = null)
    {
        $this->readAuthorisedRoles = $roles;

        return $this;
    }

    /**
     *
     * @param $role
     * @return bool
     */
    public function hasReadAuthorisedRole($role)
    {
        return in_array($role, $this->readAuthorisedRoles);
    }

    /**
     *
     * @param  AuthorizationCheckerInterface $securityContext
     * @return bool
     */
    public function isAuthorisedToRead(AuthorizationCheckerInterface $securityContext)
    {
        if (0 == count($this->readAuthorisedRoles)) {
            return true;
        }

        foreach ($this->readAuthorisedRoles as $role) {
            if ($securityContext->isGranted($role)) {
                return true;
            }
        }

        return false;
    }
}
