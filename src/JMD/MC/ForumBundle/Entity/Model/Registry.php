<?php
namespace JMD\MC\ForumBundle\Entity\Model;

use Symfony\Component\Security\Core\User\UserInterface;

abstract class Registry
{
    /** @var UserInterface $ownedBy */
    protected $ownedBy = null;

    /**
     *
     * @access public
     */
    public function __construct()
    {
        // your own logic
    }

    /**
     * Get owned_by
     *
     * @return UserInterface
     */
    public function getOwnedBy()
    {
        return $this->ownedBy;
    }

    /**
     * Set owned_by
     *
     * @param  UserInterface $ownedBy
     * @return Registry
     */
    public function setOwnedBy($ownedBy = null)
    {
        $this->ownedBy = $ownedBy;

        return $this;
    }
}
