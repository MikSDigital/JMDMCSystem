<?php
namespace JMD\MC\ForumBundle\Entity\Model;

use Doctrine\Common\Collections\ArrayCollection;

use JMD\MC\ForumBundle\Entity\Category as ConcreteCategory;

abstract class Forum
{
    /** @var $categories */
    protected $categories = null;

    /**
     *
     * @access public
     */
    public function __construct()
    {
        // your own logic
        $this->categories = new ArrayCollection();
    }

    /**
     *
     * Get categories
     *
     * @return Categories
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     *
     * Set categories
     *
     * @return Forum
     */
    public function setCategories(ArrayCollection $categories = null)
    {
        $this->categories = $categories;

        return $this;
    }

    /**
     *
     * Add category
     *
     * @param  Category $category
     * @return Forum
     */
    public function addCategory(ConcreteCategory $category)
    {
        $this->categories->add($category);

        return $this;
    }

    /**
     *
     * Remove Category
     *
     * @param  Category $category
     * @return Forum
     */
    public function removeCategory(ConcreteCategory $category)
    {
        $this->categories->removeElement($category);

        return $this;
    }
}
