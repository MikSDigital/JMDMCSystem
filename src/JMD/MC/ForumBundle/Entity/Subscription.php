<?php
namespace JMD\MC\ForumBundle\Entity;

use JMD\MC\ForumBundle\Entity\Model\Subscription as AbstractSubscription;

class Subscription extends AbstractSubscription
{
    /**
     *
     * @var integer $id
     */
    protected $id;

    /**
     *
     * @var Boolean $isRead
     */
    protected $isRead = false;

    /**
     *
     * @var Boolean $isSubscribed
     */
    protected $isSubscribed = false;

    /**
     *
     * @access public
     */
    public function __construct()
    {
        parent::__construct();
        // your own logic
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get isRead
     *
     * @return boolean
     */
    public function isRead()
    {
        return $this->isRead;
    }

    /**
     * Set isRead
     *
     * @param  boolean      $isRead
     * @return Subscription
     */
    public function setRead($isRead)
    {
        $this->isRead = $isRead;

        return $this;
    }

    /**
     * Get isSubscribed
     *
     * @return boolean
     */
    public function isSubscribed()
    {
        return $this->isSubscribed;
    }

    /**
     * Set isSubscribed
     *
     * @param  boolean      $isSubscribed
     * @return Subscription
     */
    public function setSubscribed($isSubscribed)
    {
        $this->isSubscribed = $isSubscribed;

        return $this;
    }
}
