<?php

namespace JMD\MC\DialogBundle\Controller;

use JMD\MC\CoreBundle\Controller\BaseController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class Select2AjaxController extends BaseController
{
    public function usersAction(Request $request)
    {
        $currentUser = $this->getUser();

        $blackList = $currentUser->getBlackList() === null ? [] : $currentUser->getBlackList()->toArray();

        $qb = $this->getODM()
            ->getRepository('JMDMCCoreBundle:User')
            ->createQueryBuilder();

        $users = $qb
            ->addAnd(
                $qb->expr()->field('id')->notIn([$currentUser->getId()])
            )
            ->addAnd(
                $qb->expr()->field('username')->equals(new \MongoRegex('/^' . $request->get('q') . '/'))
            )
            ->addAnd(
                $qb->expr()->field('id')->notIn($blackList)
            )
            ->getQuery()->execute()
        ;

        $response = null;
        $i = 0;
        foreach ($users as $user) {
            $response[$i] = [
                'id'    => $user->getId(),
                'text'  => $user->getUsername(),
            ];

            if (null !== $user->getAvatar()) {
                $img = $this->get('assets.packages')->getUrl($user->getAvatar()->getPath());
            } else {
                $img = $this->get('assets.packages')->getUrl('img/default_ava.jpg');
            }
            $response[$i]['img'] = $img;

            ++$i;
        }

        return new JsonResponse($response, 200);
    }
}
