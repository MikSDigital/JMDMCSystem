<?php
namespace JMD\MC\DialogBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableDocument;
use Gedmo\Timestampable\Traits\TimestampableDocument;
use JMD\MC\CoreBundle\Document\User;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Messages
 * @package JMD\MC\DialogBundle\Document
 *
 * @ODM\Document()
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class Message
{
    use SoftDeleteableDocument;
    use TimestampableDocument;

    /**
     * @var string
     * @ODM\Id()
     */
    private $id;

    /**
     * @var Dialog
     * @ODM\ReferenceOne(targetDocument="Dialog", inversedBy="messages")
     * @Assert\NotNull()
     */
    private $dialog;

    /**
     * @var User
     * @ODM\ReferenceOne(targetDocument="JMD\MC\CoreBundle\Document\User")
     */
    private $user;

    /**
     * @var string
     * @ODM\Field(type="string")
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    private $message;

    /**
     * @var bool
     * @ODM\Field(type="bool")
     */
    private $isReaded = false;

    /**
     * Get id
     *
     * @return string $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dialog
     *
     * @param Dialog $dialog
     * @return self
     */
    public function setDialog(Dialog $dialog)
    {
        $this->dialog = $dialog;
        return $this;
    }

    /**
     * Get dialog
     *
     * @return Dialog $dialog
     */
    public function getDialog()
    {
        return $this->dialog;
    }

    /**
     * Set user
     *
     * @param User $user
     * @return self
     */
    public function setUser(User $user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * Get user
     *
     * @return User $user
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set message
     *
     * @param string $message
     * @return self
     */
    public function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }

    /**
     * Get message
     *
     * @return string $message
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set isReaded
     *
     * @param bool $isReaded
     * @return self
     */
    public function setIsReaded($isReaded)
    {
        $this->isReaded = $isReaded;
        return $this;
    }

    /**
     * Get isReaded
     *
     * @return bool $isReaded
     */
    public function getIsReaded()
    {
        return $this->isReaded;
    }
}
