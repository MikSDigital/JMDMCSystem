<?php

namespace JMD\MC\DialogBundle\Form;

use Doctrine\Bundle\MongoDBBundle\Form\Type\DocumentType;
use Doctrine\ODM\MongoDB\DocumentRepository;
use JMD\MC\CoreBundle\Form\Type\Select2DocumentType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class Dialog extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('users', Select2DocumentType::class, [
                'class' => 'JMD\MC\CoreBundle\Document\User',
                'multiple' => true,
                'primary_key' => '_id',
                'remote_route' => 'dialog_select2_user',
                'label' => 'dialog.form.new.user',
                'text_property' => 'username'
            ])
            ->add('theme', TextType::class, [
                'label' => 'dialog.form.new.theme'
            ])
            ->add('message', TextareaType::class, [
                'label' => 'dialog.form.new.message',
                'attr'  => [
                    'rows'  => 10
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults([
            'translation_domain'    => 'dialog'
        ]);
    }
}
