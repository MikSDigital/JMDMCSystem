<?php

namespace JMD\MC\CoreBundle\Form;

use JMD\MC\CoreBundle\Form\Type\Select2DocumentType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BlacklistType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('user', Select2DocumentType::class, [
                'class'         => 'JMD\MC\CoreBundle\Document\User',
                'multiple'      => false,
                'primary_key'   => '_id',
                'remote_route'  => 'dialog_select2_user',
                'label'         => 'user.blacklist.form.title',
                'text_property' => 'username',
                'allow_clear'   => false,
                'placeholder'   => 'user.blacklist.form.placeholder'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults([
            'translation_domain'    => 'profile'
        ]);
    }
}
