<?php
namespace JMD\MC\CoreBundle\Form\UserProfile;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SettingsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('show_sign', CheckboxType::class, [
                'label'     => 'user.form.settings.show_sign',
                'required'  => false
            ])
            ->add('show_email', CheckboxType::class, [
                'label' => 'user.form.settings.show_email',
                'required' => false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults([
            'translation_domain'    => 'profile'
        ]);
    }
}
