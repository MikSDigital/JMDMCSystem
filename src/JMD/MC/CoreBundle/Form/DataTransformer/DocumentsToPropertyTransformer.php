<?php

namespace JMD\MC\CoreBundle\Form\DataTransformer;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\DBAL\Exception\DriverException;
use Doctrine\ODM\MongoDB\DocumentManager;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Symfony\Component\PropertyAccess\PropertyAccess;

/**
 * Data transformer for multiple mode (i.e., multiple = true)
 *
 * Class EntitiesToPropertyTransformer
 * @package Tetranz\Select2EntityBundle\Form\DataTransformer
 */
class DocumentsToPropertyTransformer implements DataTransformerInterface
{
    /** @var DocumentManager */
    protected $dm;
    /** @var  string */
    protected $className;
    /** @var  string */
    protected $textProperty;
    /** @var  string */
    protected $primaryKey;

    /**
     * @param DocumentManager $dm
     * @param string $class
     * @param string|null $textProperty
     * @param string $primaryKey
     */
    public function __construct(DocumentManager $dm, $class, $textProperty = null, $primaryKey = 'id')
    {
        $this->dm = $dm;
        $this->className = $class;
        $this->textProperty = $textProperty;
        $this->primaryKey = $primaryKey;
    }

    /**
     * Transform initial entities to array
     *
     * @param mixed $documents
     * @return array
     */
    public function transform($documents)
    {
        if (is_null($documents) || count($documents) === 0) {
            return array();
        }

        $data = array();

        $accessor = PropertyAccess::createPropertyAccessor();

        foreach ($documents as $document) {
            $text = is_null($this->textProperty)
                ? (string) $document
                : $accessor->getValue($document, $this->textProperty);

            $data[$accessor->getValue($document, $this->primaryKey)] = $text;
        }

        return $data;
    }

    /**
     * Transform array to a collection of entities
     *
     * @param array $values
     * @return ArrayCollection
     */
    public function reverseTransform($values)
    {
        if (!is_array($values) || count($values) === 0) {
            return new ArrayCollection();
        }

        try {
          // get multiple documents with one query
            $documents = $this->dm
                ->getRepository($this->className)
                ->createQueryBuilder()
                ->field($this->primaryKey)->in($values)
                ->getQuery()
                ->execute();
        }
        catch (DriverException $ex) {
          // this will happen if the form submits invalid data
          throw new TransformationFailedException('One or more id values are invalid');
        }

        return $documents;
    }
}
