<?php
namespace JMD\MC\CoreBundle\Service\Helper;

use JMD\MC\CoreBundle\Service\Settings;
use Swift_Mailer;
use Twig_Environment;

class Mailer
{
    /** @var  Swift_Mailer */
    private $mailer;

    /** @var Twig_Environment */
    private $twig;

    /** @var Settings */
    private $settings;

    /** @var string */
    private $emailFrom;

    /** @var string */
    private $message;

    /** @var string */
    private $subject;

    /**
     * Mailer constructor.
     * @param Swift_Mailer $mailer
     */
    public function __construct(Swift_Mailer $mailer, Twig_Environment $twig, Settings $settings)
    {
        $this->mailer = $mailer;
        $this->twig = $twig;
        $this->settings = $settings;

        $contactEmail = $this->settings->get('contact_email');
        $this->emailFrom = $contactEmail === null ? null : $contactEmail->getValue();
    }

    /**
     * Send email
     *
     * @param string $to
     * @param string $viewTemplate
     */
    public function sendEmail($to, $viewTemplate)
    {
        $from = $this->getEmailFrom();
        $subject = $this->getSubject();
        $message = $this->twig->render($viewTemplate, [
            'message' => $this->getMessage()
        ]);

        if ($message && !empty($message)) {
            $composeMessage = $this->mailer->createMessage()
                ->setSubject($subject)
                ->setTo($to)
                ->setFrom($from)
                ->setBody(
                    $message,
                    'text/html',
                    'UTF-8'
                )
            ;

            $this->mailer->send($composeMessage);
        }
    }

    /**
     * @return string
     */
    public function getEmailFrom()
    {
        return $this->emailFrom;
    }

    /**
     * @param string $emailFrom
     * @return self
     */
    public function setEmailFrom($emailFrom)
    {
        $this->emailFrom = $emailFrom;

        return $this;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $message
     * @return self
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param mixed $subject
     * @return self
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }
}