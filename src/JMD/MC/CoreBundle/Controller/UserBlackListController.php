<?php
namespace JMD\MC\CoreBundle\Controller;

use JMD\MC\CoreBundle\Document\User;
use JMD\MC\CoreBundle\Form\BlacklistType;
use Symfony\Component\HttpFoundation\Request;

class UserBlackListController extends BaseController
{
    /**
     * Add user to blacklist
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $form = $this->createForm(BlacklistType::class);

        if ($request->getMethod() === 'POST') {
            $form->handleRequest($request);
            if (!$form->isValid()) {
                foreach ($form->getErrors() as $error) {
                    $this->addFlash('danger', $this->trans($error->getMessage(), [], 'profile'));
                }

                return $this->render(':UserProfile:blacklist.html.twig', [
                    'current' => 'blacklist',
                    'title' => $this->trans('title.profile.blacklist'),
                    'form' => $form->createView()
                ]);
            }

            $data = $form->getData();

            if (!isset($data['user']) || empty($data['user'])) {
                $this->addFlash('danger', $this->trans('user.blacklist.form.error.no_user', [], 'profile'));

                return $this->render(':UserProfile:blacklist.html.twig', [
                    'current' => 'blacklist',
                    'title' => $this->trans('title.profile.blacklist'),
                    'form' => $form->createView()
                ]);
            }

            /** @var User $user */
            $user = $this->getUser();
            $user->addBlackList($data['user']);

            $this->getODM()->persist($user);
            $this->getODM()->flush();

            $this->addFlash('success', $this->trans('user.blacklist.form.success', [], 'profile'));

            return $this->redirectToRoute('user_blacklist');
        }

        return $this->render(':UserProfile:blacklist.html.twig', [
            'current'   => 'blacklist',
            'title'     => $this->trans('title.profile.blacklist'),
            'form'      => $form->createView()
        ]);
    }

    public function removeAction($id)
    {
        $whiteListed = $this->getODM()->getRepository('JMDMCCoreBundle:User')->find($id);

        if ($whiteListed === null) {
            $this->addFlash('danger', $this->trans('user.blacklist.not_found', [], 'profile'));

            return $this->redirectToRoute('user_blacklist');
        }

        /** @var User $user */
        $user = $this->getUser();
        if ($user->isInBlackList($whiteListed)) {
            $user->removeBlackList($whiteListed);
        }

        $this->getODM()->persist($user);
        $this->getODM()->flush();

        $this->addFlash('success', $this->trans('user.blacklist.whitelisted', [], 'profile'));

        return $this->redirectToRoute('user_blacklist');
    }
}
