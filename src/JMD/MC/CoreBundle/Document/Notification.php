<?php
namespace JMD\MC\CoreBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Notification
 * @package JMD\MC\CoreBundle\Document
 *
 * @ODM\Document()
 */
class Notification
{
    /**
     * @var string
     * @ODM\Id()
     */
    private $id;

    /**
     * @var object|null
     * @ODM\ReferenceOne(nullable=true)
     */
    private $source;

    /**
     * @var string
     * @ODM\Field()
     */
    private $routeName;

    /**
     * @var string
     * @ODM\Field()
     */
    private $message;

    /**
     * @var User
     * @ODM\ReferenceOne(targetDocument="User", nullable=false)
     * @Assert\NotNull()
     */
    private $user;

    /**
     * @var object
     * @ODM\ReferenceOne(nullable=false)
     * @Assert\NotNull()
     */
    private $destination;

    /**
     * Get id
     *
     * @return string $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set source
     *
     * @param object|null $source
     * @return self
     */
    public function setSource($source = null)
    {
        $this->source = $source;
        return $this;
    }

    /**
     * Get source
     *
     * @return object|null $source
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * Set user
     *
     * @param User $user
     * @return self
     */
    public function setUser(User $user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * Get user
     *
     * @return User $user
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set destination
     *
     * @param object $destination
     * @return self
     */
    public function setDestination($destination)
    {
        $this->destination = $destination;
        return $this;
    }

    /**
     * Get destination
     *
     * @return object $destination
     */
    public function getDestination()
    {
        return $this->destination;
    }

    /**
     * Set routeName
     *
     * @param string $routeName
     * @return self
     */
    public function setRouteName($routeName)
    {
        $this->routeName = $routeName;
        return $this;
    }

    /**
     * Get routeName
     *
     * @return string $routeName
     */
    public function getRouteName()
    {
        return $this->routeName;
    }

    /**
     * Set message
     *
     * @param string $message
     * @return self
     */
    public function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }

    /**
     * Get message
     *
     * @return string $message
     */
    public function getMessage()
    {
        return $this->message;
    }
}
