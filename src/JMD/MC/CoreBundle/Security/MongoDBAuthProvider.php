<?php
namespace JMD\MC\CoreBundle\Security;

use Doctrine\ODM\MongoDB\DocumentManager;
use JMD\MC\CoreBundle\Document\User;
use Psr\Log\LoggerInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class MongoDBAuthProvider implements UserProviderInterface
{
    /** @var DocumentManager */
    private $odm;

    /** @var LoggerInterface */
    private $logger;

    /**
     * MongoDBAuthProvider constructor.
     * @param DocumentManager $odm
     * @param LoggerInterface $logger
     */
    public function __construct(DocumentManager $odm, LoggerInterface $logger)
    {
        $this->odm = $odm;
        $this->logger = $logger;
    }

    /**
     * @param string $username
     * @return User|null
     */
    public function loadUserByUsername($username)
    {
        $this->logger->debug('User load request. name: '. $username);
        $user = $this->odm->getRepository('JMDMCCoreBundle:User')->loadUserByUsername($username);

        if (null === $user) {
            throw new UsernameNotFoundException(sprintf('User "%s" does not exist.', $username));
        }

        return $user;
    }

    /**
     * @param UserInterface $user
     * @return User
     * @throws \Doctrine\ODM\MongoDB\LockException
     */
    public function refreshUser(UserInterface $user)
    {
        if (!($user instanceof User)) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', get_class($user)));
        }

        $this->logger->info("Refresh user from mongo: '".$user->getUsername()."'");
        $_user = $this->odm->getRepository('JMDMCCoreBundle:User')->find($user->getId());

        if ($_user && $_user instanceof User) {
            $this->logger->info('User "'.$user->getUsername().'" roles: ' . implode(', ', $_user->getRoles()));
        } else {
            throw new UsernameNotFoundException(sprintf('User "%s" does not exist.', $user->getUsername()));
        }

        return $_user;
    }

    public function supportsClass($class)
    {
        $this->logger->debug("Support checking: $class");

        if ($class == 'JMD\MC\CoreBundle\Document\User') {
            return true;
        }

        return false;
    }
}