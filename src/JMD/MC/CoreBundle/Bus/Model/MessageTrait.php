<?php
namespace JMD\MC\CoreBundle\Bus\Model;

trait MessageTrait
{
    public function __construct(array $values = [], array $extra = [])
    {
        $empty2null = function ($value) use (&$empty2null) {

            if (is_array($value)) {
                foreach ($value as &$v) {
                    $v = $empty2null($v);
                }

                return $value;
            }

            return is_string($value) && strlen($value) === 0 ? null : $value;
        };

        $data = $empty2null($extra + $values);

        foreach ($data as $property => $value) {
            if (property_exists($this, $property)) {
                $this->$property = $value;
            }
        }
    }
}