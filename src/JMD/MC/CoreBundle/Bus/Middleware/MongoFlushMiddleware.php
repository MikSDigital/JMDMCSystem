<?php
namespace JMD\MC\CoreBundle\Bus\Middleware;

use Doctrine\ODM\MongoDB\DocumentManager;
use SimpleBus\Message\Bus\Middleware\MessageBusMiddleware;

class MongoFlushMiddleware implements MessageBusMiddleware
{
    /** @var DocumentManager */
    public $odm;

    /**
     * MongoFlushMiddleware constructor.
     * @param DocumentManager $odm
     */
    public function __construct(DocumentManager $odm)
    {
        $this->odm = $odm;
    }

    public function handle($message, callable $next)
    {
        $next($message);
        $this->odm->flush();
    }
}