<?php
namespace JMD\MC\CoreBundle\Twig;

use JMD\MC\CoreBundle\Service\Settings;

class SettingsExtension extends \Twig_Extension implements \Twig_Extension_GlobalsInterface
{
    /** @var Settings */
    private $settings;

    /**
     * SettingsExtension constructor.
     * @param Settings $settings
     */
    public function __construct(Settings $settings)
    {
        $this->settings = $settings;
    }

    public function getGlobals()
    {
        return [
            'jmd_settings' => $this->settings
        ];
    }

    public function getName()
    {
        return self::class;
    }
}