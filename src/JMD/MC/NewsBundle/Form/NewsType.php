<?php

namespace JMD\MC\NewsBundle\Form;

use JMD\MC\NewsBundle\Form\Type\TagsType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class NewsType extends AbstractType
{
    /** @var AuthorizationCheckerInterface */
    private $security;

    /**
     * NewsType constructor.
     * @param AuthorizationCheckerInterface $security
     */
    public function __construct(AuthorizationCheckerInterface $security)
    {
        $this->security = $security;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'label'     => 'news.form.title'
            ])
            ->add('tags', TagsType::class, [
                'label'     => 'news.form.tags',
                'required'  => false
            ])
            ->add('shortContent', TextareaType::class, [
                'label'     => 'news.form.short',
                'required'  => false,
                'attr'      => [
                    'rows'  => 5
                ]
            ])
            ->add('fullContent', TextareaType::class, [
                'label' => 'news.form.full',
                'attr'  => [
                    'rows' => 10
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults([
            'data_class'            => 'JMD\MC\NewsBundle\Document\News',
            'translation_domain'    => 'news'
        ]);
    }
}
