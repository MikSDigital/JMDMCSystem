<?php
namespace JMD\MC\NewsBundle\Bus\Command;

use JMD\MC\CoreBundle\Bus\Model\MessageTrait;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class RemoveCommentCommand
 * @package JMD\MC\NewsBundle\Bus\Command
 */
class RemoveCommentCommand
{
    use MessageTrait;

    /**
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    public $id;
}